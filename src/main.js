import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueScrollTo from 'vue-scrollto'
import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify)

Vue.use(VueScrollTo)

Vue.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
})

new Vue({
  el: '#app',
  render: h => h(App)
})
